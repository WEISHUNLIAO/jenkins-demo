import boto3
from flask import Flask, render_template, request, redirect, jsonify
from flask_mysqldb import MySQL

app = Flask(__name__)
# CORS(app)
app.config['MYSQL_HOST'] = 'database-1.cwkrfnle04ii.eu-west-1.rds.amazonaws.com'
app.config['MYSQL_USER'] = 'root'
app.config['MYSQL_PASSWORD'] = 'a00000000'
app.config['MYSQL_DB'] = ' event_organiser'
mysql = MySQL(app)


@app.route('/')
def index():
    return render_template('index.html')


@app.route('/edit')
def edit():
    edit_type = request.args.get('type')
    event_id = request.args.get('id')
    if event_id is None:
        event_id = 999

    return render_template('edit.html', type=edit_type, id=event_id)


@app.route('/archive')
def archive():
    return render_template('archive.html')


@app.route('/event/<event_id>')
def event_page(event_id):
    return render_template('event_page.html', event_id=event_id)


@app.route('/api/events')
def select_all():
    cur = mysql.connection.cursor()
    cur.execute("Select * from event")
    mysql.connection.commit()
    data = cur.fetchall()
    cur.close()
    payload = []
    for result in data:
        content = {'id': result[0], 'title': result[1], 'start': result[2], 'end': result[3], 'location': result[4],
                   'photo': result[5],
                   'detail': result[6], 'price': result[7], 'archive': result[8]}
        payload.append(content)

    return jsonify(payload)


@app.route('/api/archive_events')
def select_archive():
    cur = mysql.connection.cursor()
    cur.execute("Select * from event")
    mysql.connection.commit()
    data = cur.fetchall()
    cur.close()
    payload = []
    for result in data:
        if result[8] == 1:
            content = {'id': result[0], 'title': result[1], 'start': result[2], 'location': result[4]}
            payload.append(content)
    return jsonify(payload)


@app.route('/api/event', methods=['GET', 'POST', 'PUT', 'DELETE'])
def insert():
    if request.method == 'GET':
        event_id = request.args.get('id')
        cur = mysql.connection.cursor()
        cur.execute(
            "Select id, title,DATE_FORMAT(`start`,'%Y-%m-%d %H:%i'),DATE_FORMAT(`end`,'%Y-%m-%d %H:%i'),location,detail,price,photo from event where id=" + event_id)
        mysql.connection.commit()
        result = cur.fetchall()[0]
        content = {'title': result[1], 'start': result[2], 'end': result[3], 'location': result[4],
                   'detail': result[5], 'price': result[6], 'photo' : result[7]}
        cur.close()
        return jsonify(content)

    if request.method == 'POST':
        result = request.form
        file = request.files['Image']

        cur = mysql.connection.cursor()
        if result['_METHOD'] == 'PUT':
            if file.filename == "":
                cur.execute((
                                "UPDATE event SET title= %s, location = %s, start = %s, end = %s, price = %s, detail = %s WHERE id=%s"),
                            (
                                result['title'], result['location'], result['date_picker_start'],
                                result['date_picker_end'],
                                result['price'],
                                result['detail'], result['id']))
            else:
                url = upload_photo(file)
                cur.execute((
                                "UPDATE event SET title= %s, location = %s, start = %s, end = %s, price = %s, detail = %s, photo = %s WHERE id=%s"),
                            (
                                result['title'], result['location'], result['date_picker_start'],
                                result['date_picker_end'],
                                result['price'],
                                result['detail'], url, result['id']))
        else:
            url = upload_photo(file)
            cur.execute(
                "INSERT INTO event(title, start, end, location, price, detail, archive, photo) VALUES (%s, %s,%s,%s,%s,%s,%s,%s)",
                (
                    result['title'], result['date_picker_start'], result['date_picker_end'], result['location'],
                    result['price'],
                    result['detail'],
                    0, url))
        mysql.connection.commit()
        cur.close()
        return redirect("/")

    if request.method == 'PUT':
        event_id = request.args['id']
        arch = request.args['archive']
        cur = mysql.connection.cursor()
        if arch is not None:
            cur.execute("UPDATE event SET archive=" + arch + " WHERE id=" + event_id)
        else:
            result = request.form
            cur.execute((
                            "UPDATE event SET title= %s, location = %s, start = %s, end = %s, price = %s, detail = %s  WHERE id=%s"),
                        (
                            result['title'], result['location'], result['date_picker_start'], result['date_picker_end'],
                            result['price'],
                            result['detail']))

        mysql.connection.commit()
        cur.close()
        return "OK"

    if request.method == 'DELETE':
        event_id = request.args['id']
        cur = mysql.connection.cursor()
        cur.execute("DELETE FROM event WHERE id=" + event_id)
        mysql.connection.commit()
        cur.close()
        return "OK"


def upload_photo(file):
    s3_client = boto3.client('s3', aws_access_key_id="AKIAZMPKILOZJOSBMEUQ",
                             aws_secret_access_key="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")
    s3_client.upload_fileobj(file, 'eventscc2019', "photo/" + file.filename,
                             ExtraArgs={'ACL': 'public-read', "ContentType": file.content_type})
    url = "https://eventscc2019.s3-eu-west-1.amazonaws.com/photo/" + file.filename
    return url


if __name__ == '__main__':
    app.run(host='0.0.0.0')
