let substringMatcher = function (strs) {
    return function findMatches(q, cb) {
        let matches, substringRegex;

        // an array that will be populated with substring matches
        matches = [];

        // regex used to determine if a string contains the substring `q`
        substrRegex = new RegExp(q, 'i');

        // iterate through the pool of strings and for any string that
        // contains the substring `q`, add it to the `matches` array
        $.each(strs, function (i, str) {
            if (substrRegex.test(str)) {
                matches.push(str);
            }
        });

        cb(matches);
    };
};


fetch('/api/events', {
    method: 'GET',
}).then((resp) => {
    return resp.json();
}).then((data) => {
    let events = [];
    for (let i of data) {
        if (i['archive'] === 0) {
            events.push(i['title']);
        }
    }
    return events;
}).then((events) => {
    $('#search_box .typeahead').typeahead({
            hint: true,
            highlight: true,
            minLength: 1
        },
        {
            name: 'states',
            source: substringMatcher(events)
        }
    );
});

